# dotfiles

## References

- [hlissner/doom-emacs](https://github.com/hlissner/doom-emacs)
- [zsh-users/antigen](https://github.com/zsh-users/antigen)
- [XDG Base Directory - ArchWiki](https://wiki.archlinux.org/index.php/XDG_Base_Directory)
