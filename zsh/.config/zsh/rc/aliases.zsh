alias ls='exa'
alias ll='exa -alFh --git --time-style=long-iso'

alias cp='cp -i'
alias mv='mv -i'
alias rm='rm -i'

alias reload="exec ${SHELL} -l"
alias path='echo -e ${PATH//:/\\n}'
