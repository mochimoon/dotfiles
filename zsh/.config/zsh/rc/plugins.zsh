export ADOTDIR=$XDG_DATA_HOME/antigen
source $XDG_DATA_HOME/antigen/antigen.zsh

antigen bundle zsh-users/zsh-completions
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle zsh-users/zsh-history-substring-search
antigen bundle zsh-users/zsh-autosuggestions

antigen bundle mafredri/zsh-async
antigen bundle sindresorhus/pure
PURE_PROMPT_SYMBOL="%(?!(._.)/!(;_;%)?)"

antigen apply
