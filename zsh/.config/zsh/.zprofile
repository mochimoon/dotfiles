export GPG_TTY=$(tty)

export PATH="$PATH:$HOME/.local/bin"

# Rust
export RUSTUP_HOME="$XDG_DATA_HOME/rustup"
export CARGO_HOME="$XDG_DATA_HOME/cargo"
export PATH="$PATH:$CARGO_HOME/bin"

# wget
export WGETRC="$XDG_CONFIG_HOME/wgetrc"

# Linuxbrew
eval $(/home/linuxbrew/.linuxbrew/bin/brew shellenv)

# Doom Emacs
export PATH="$PATH:$HOME/.emacs.d/bin"
