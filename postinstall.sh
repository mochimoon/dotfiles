function setup_dir_and_file() {
    mkdir -p $XDG_CONFIG_HOME
    mkdir -p $XDG_CACHE_HOME
    mkdir -p $XDG_DATA_HOME
    mkdir -p $XDG_DATA_HOME/zsh
    if [ ! -f $HISTFILE ]; then
        touch $HISTFILE
    fi
}

function setup_zsh() {
    git clone https://github.com/zsh-users/antigen $XDG_DATA_HOME/antigen
}

function setup_emacs() {
    git clone https://github.com/hlissner/doom-emacs $HOME/.emacs.d
    $HOME/.emacs.d/bin/doom install
}

function setup_rust() {
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- --no-modify-path -y
}

function postinstall() {
    setup_dir_and_file
    setup_zsh
    setup_emacs
    setup_rust
}

postinstall
