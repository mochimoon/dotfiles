DOTFILES_PATH="$HOME/dotfiles"
XDG_DATA_HOME="$HOME/.local/share"

function install_packages() {
    sudo apt -y update
    sudo apt -y upgrade
    sudo apt -y install build-essential curl file git zsh
}

function install_linuxbrew() {
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/Linuxbrew/install/master/install.sh)"
}

function fetch_repository() {
    git clone https://gitlab.com/mochimoon/dotfiles $DOTFILES_PATH
}

function install_bundle() {
    eval $(/home/linuxbrew/.linuxbrew/bin/brew shellenv)
    brew bundle install --file=$DOTFILES_PATH/Brewfile
}

function link() {
    stow -d $DOTFILES_PATH -t $HOME -v zsh wget git emacs
}

function setup_zsh() {
    sudo chsh $USER -s `which zsh`
    git clone https://github.com/zsh-users/antigen $XDG_DATA_HOME/antigen
}

function install() {
    install_packages
    install_linuxbrew
    fetch_repository
    install_bundle
    link
    setup_zsh
}

install
